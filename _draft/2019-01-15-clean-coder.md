---
type: single
title: 'Clean Code - by Robert Martin: Summary'
permalink: /typescript-summary/
excerpt: "A brief summary of the book `Clean Coder by Robert Martin`"
header:
  image: '/assets/images/clean-code-head.jpg'
  teaser: '/assets/images/clean-code-head.jpg'
author_profile: false
comments: true
draft: true
---

An insightful book that talks about how to be a professional developer. Encouraging you to take responsibility of your career to be better.

## Professionalism

> Professionalism is all about taking responsibility. — Robert Martin

Don’t take pride or honor in something that you won’t be held accountable for.

>
>
