---
type: page
title: ""
permalink: /about/
author_profile: false
header:
  image: "/assets/images/about-color.jpg"
---

# Hi! I'm André Hatlo-Johansen!

## Professionally:

A developer, worked on an ecommerce project for Rørkjøp Nett AS for 2 years, and worked on a WebBank project for Nordea for about 1 year which entailed building API microservices.

### Rørkjøp

Hired me after writing my bachelor thesis on [Machine Learning in an E-Commercial Business](https://drive.google.com/open?id=1ZTvTA2Uo2WnMI_grts0aJcPWlR_Hje06) for Rørkjøp AS.

On this thesis my main job in the group was to implement a working machine learning solution for Rørkjøp AS.

This entailed setting up a machine learning system that learned by looking at personalized data sets for each unique customer, based on their previous shopping pattern and return personal product recommendations. Which it did successfully.

    Technologies utilized for thesis:
      - Linux
      - Ruby
      - Ruby on Rails
      - Postgresql
      - Amazon AWS
      - ApacheSpark

#### Working here I've become:

     Very comfortable with:
      - Ruby
      - Ruby On Rails
      - Rspec
      - Slim
      - Scss
      - Postgresql

    Moderately comfortable with:
      - Javascript
      - Elasticsearch
      - Heroku

Projects I've been assigned while working for Rørkjøp are mostly based on creating features too speed up productivity, scalability or maintainability of Rørkjøp's e-commerce platform.

#### Nordea AS

My second job at Nordea AS i was stationed mostly backend, building microservice API's.
Working here i've utilized Java foremost with reactive libraries to create rich streaming applications:

Technologies used at Nordea AS:

- Java
  - RxJava library
  - Reactor
  - SpringBoot
- Maven
- SQL
- IntelliJ (IDE)

### Other skills:

    - Java (Upper Intermediate)
    - HTML (Advanced)
    - CSS/SCSS  (Pre-advanced)
    - Adobe Photoshop (Pre-advanced)
    - Linux/Windows/iOS (Advanced)

On my spare time i like to **solve puzzles**, **read**, **gain knowledge** and **skills in different technologies** available while keeping an eye open for new trends to explore.

### E-Commerce and Digital Marketing

Having my own e-commerce site and trying to gain organic growth has made me efficient at SEO and website structuring for website traffic. Which again forces me to be creative and use some of my spare time to write articles about subjects that i've never thought about.

Also i am very comfortable with Facebook Ads and Google Ads, which i've used to convert customers.

[Take a look at my CV for additional information](https://drive.google.com/file/d/1YhH8U9hb2Ym1GSNY357iKamTAMfcBrCV/view)

## On a personal note:

I'm a 27 year old man who both loves a good laugh, having a good time and values my alone time.

At the moment I live in Drammen Norway, with my best friend and beautiful partner Hilde.

Together we have a super funny thirty-five kilo, three year old boxer named **Mini**.

<img src="/assets/images/fam.jpg" alt="my family"/>

## Hobbies:

- Cooking - I love to both make food and eat
- Ski, snowboarding.
- Keeping up with my favourite tv-shows.
- Playing and walking with my dog.
- I've picked up Yoga in the last year.

## Phrases i live by:

1. Don't take life too seriously.

2. Live each day as it would be your last.

3. Love what you do, not what you earn.

4. Make friends, not enemies.

5. Let the universe do the worrying for you.

6. Last but not least, enjoy the ride.
   - You only get **ONE!**
