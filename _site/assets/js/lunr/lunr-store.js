var store = [{
        "title": "Jekyll: Creating a blog is easy and FREE",
        "excerpt":"So, i’ve chosen to start blogging my experiences/insights/thoughts as a developer. Both to learn and to share knowledge. Trying to find the right platform to start a blog was my biggest challenge. Google didn’t make in much easier, i have a feeling their algorithm favorites something called WORDPRESS. So i...","categories": [],
        "tags": [],
        "url": "http://localhost:4000/jekyll-creating-blog/",
        "teaser":"http://localhost:4000/assets/images/jekyll-head.jpg"},{
        "title": "Jekyll + Github = Free hosting: Tutorial",
        "excerpt":"Requirements: Terminal Check whether you have Ruby 2.1.0 or higher installed: $ ruby -v ruby 2.x.x Install Bundler: $ gem install bundler Create a local repository for your Jekyll site. (Jump to step 1 if this is done) Install git, see set up Git Open terminal Initialize a new Git...","categories": [],
        "tags": [],
        "url": "http://localhost:4000/jekyll-github-free-host/",
        "teaser":"http://localhost:4000/assets/images/jekyllgit.jpg"},{
        "title": "Hugo - as my static page blog",
        "excerpt":"After the setup of my Jekyll site I came across Hugo. Hugo is a static site generator, on the same line as Jekyll as it uses Markdown to create blog posts. The main difference i noticed after testing Jekyll against Hugo is that it’s fast. It seems like it has...","categories": [],
        "tags": [],
        "url": "http://localhost:4000/hugo/",
        "teaser":"http://localhost:4000/assets/images/hugogit.jpg"},{
        "title": "TypeScript syntax summary",
        "excerpt":"TypeScript is one of the fastest rising technologies of 2018. This brief summary is filled with what you need to know to understand its key concepts. TypeScript is built by Microsoft, by one of its creators Anders Hejlsberg, a danish software engineer known for Turbo Pascal and Delphi. Its open...","categories": [],
        "tags": [],
        "url": "http://localhost:4000/typescript-summary/",
        "teaser":"http://localhost:4000/assets/images/ts.jpg"},{
        "title": "Clean Coder - Robert Martin: Summary",
        "excerpt":"An insightful book that talks about how to be a professional developer. Encouraging you to take responsibility of your career to be better.   Professionalism      Professionalism is all about taking responsibility. — Robert Martin   ","categories": [],
        "tags": [],
        "url": "http://localhost:4000/typescript-summary/",
        "teaser":"http://localhost:4000/assets/images/clean-code-head.jpg"}]
